from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return "¡Bienvenido a la App espero y te guste, lo hice con mucho esfuerzo!"

if __name__ == '__main__':
    app.run()
